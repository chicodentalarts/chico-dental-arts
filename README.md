Expert level, compassionate dentistry. We are on the cutting edge of dental treatments. From General Dentistry to Cosmetic Procedures. Offering implants, same day crowns, whitening, dentures, clear braces, veneers, periodontal surgery and more! Among Chico's top dentists.

Address: 2539 Forest Ave, Chico, CA 95928, USA

Phone: 530-342-6064

Website: http://chicodentalarts.com
